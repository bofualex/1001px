//
//  ViewCell.swift
//  100pics
//
//  Created by Alexx on 22/02/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit


class ViewDetailCell: UITableViewCell{
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var thumbnailImageView2: UIImageView!
    @IBOutlet var textField1: UITextField!
    @IBOutlet var textField2: UITextField!
    @IBOutlet var votesCount: UILabel!
    @IBOutlet var userCity: UILabel!
    @IBOutlet var userCountry: UILabel!
    @IBOutlet var dataPozei: UILabel!
    @IBOutlet var cameraModel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        thumbnailImageView2.layer.cornerRadius = 15.0
        thumbnailImageView2.layer.masksToBounds = true
    }
    
}
