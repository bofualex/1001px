//
//  ViewController.swift
//  100pics
//
//  Created by Alexx on 22/02/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import UIKit
import Foundation


class PicturesCollectionController: UICollectionViewController, ManagerDelegate, UIPopoverPresentationControllerDelegate, UIActionSheetDelegate  {
    
    var photos = NSOrderedSet()
    let  manager = Manager()
    var photo: Pictures!
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Un fel de 500px"
        print("view did load\n")
        manager.delegate = self
        print("view did load -> request picture\n")
        manager.getHomeScreenImages(5)
        print("view did load -> reload data\n")
        self.collectionView!.reloadData()
        print("view did load -> gata\n")
        
    }
    
    // MARK: Tableview methods
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cellIdentifier = "PictureCell"
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! PictureCollectionCell
        photo = photos[indexPath.item] as! Pictures
        manager.getPhoto(cell.thumbnailImageView, url: photo.url)
        
        return cell
        
    }
    
    //MARK: Action methods
    
    @IBAction func showActionSheetTapped(sender: AnyObject) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Sortati dupa", message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelAction)
        
        let nudeCategory: UIAlertAction = UIAlertAction(title: "Nude", style: .Default) { action -> Void in
            self.manager.getNude(3)
        }
        actionSheetController.addAction(nudeCategory)
        
        let landscapesCategory: UIAlertAction = UIAlertAction(title: "Landscapes", style: .Default) { action -> Void in
            self.manager.getLandScapes(3)
        }
        actionSheetController.addAction(landscapesCategory)
        
        let natureCategory: UIAlertAction = UIAlertAction(title: "Nature", style: .Default) { action -> Void in
            self.manager.getNature(3)
        }
        actionSheetController.addAction(natureCategory)
        
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func showAllPhotos(sender: AnyObject) {
        self.manager.getHomeScreenImages(3)
    }
    
    // MARK: ManagerDelegate
    func managerGotList(list: NSOrderedSet) {
        photos = list
        collectionView!.reloadData()
    }
    
    // MARK: Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            let destinationController = segue.destinationViewController as! DetailViewController
            destinationController.detaliiPoza = photos[(collectionView?.indexPathsForSelectedItems()![0].item)!] as! Pictures
            
        }
        
    }
}

