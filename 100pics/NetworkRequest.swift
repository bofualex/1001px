

import Foundation
import Alamofire
import UIKit


protocol NetworkRequestDelegate: class {
    func networkRequestDoneGettingList(list: NSMutableOrderedSet)
}

class NetworkRequest{
    
    weak var delegate: NetworkRequestDelegate?
    
    func requestPicture(params: [String: AnyObject]) {
        print("network -> inceput get photos\n")
        Alamofire.request(.GET, "https://api.500px.com/v1/photos", parameters: params).responseJSON() {
            
            response in
            print("network -> get photos -> am primit rasp\n")
            let photos = NSMutableOrderedSet()
            
            if let JSON = response.result.value {
                
                print(JSON)
                let photoInfos = (JSON.valueForKey("photos") as! [NSDictionary]).filter{
                    ($0["latitude"] is NSNumber) && ($0["camera"] is String) }.map {
                        Pictures(photoID: $0["id"] as! Int, url: $0["image_url"] as! String, name: $0["name"] as! String, username: ($0["user"] as! NSDictionary)["username"] as! String, latitude: $0["latitude"] as! Double, longitude: $0["longitude"] as! Double, votes: $0["votes_count"] as! Int, city: ($0["user"] as! NSDictionary)["city"] as! String, country: ($0["user"] as! NSDictionary)["country"] as! String, dataPozei: ($0["created_at"] as! String), cameraModel: ($0["camera"] as! String))
                }
                photos.addObjectsFromArray(photoInfos)
            }
            
            print("network -> get photos -> inainte de delegat\n")
            self.delegate?.networkRequestDoneGettingList(photos)
            print("network -> get photos dupa delegat\n")
        }
        
        print("network -> sfarsit get photos\n")
    }
    
    func populatePhoto(url: String, completion:(UIImage?) -> ()) {
        Alamofire.request(.GET, url).response() {
            
            (request, response, data, error) in
            completion(UIImage(data: data!))
        }
    }

}

