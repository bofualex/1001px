import UIKit
import MapKit

// de facut curat in viewdidload

class MapViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet var mapView:MKMapView!
    
    var locatiaPozei: Pictures!
    var imageView: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Locatia pozei"
        coordonatelepozei()
    }
    
    func coordonatelepozei(){
        let coordonates = CLLocation(latitude: locatiaPozei.latitude, longitude: locatiaPozei.longitude)
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(coordonates, completionHandler: { placemarks, error in
            if error != nil {
                print(error)
                return
            }
            
            if let placemarks = placemarks {
                let placemark = placemarks[0]
                let annotation = MKPointAnnotation()
                annotation.title = self.locatiaPozei.name
                annotation.subtitle = self.locatiaPozei.username
                
                if let location = placemark.location {
                    annotation.coordinate = location.coordinate
                    
                    self.mapView.showAnnotations([annotation], animated: true)
                    self.mapView.selectAnnotation(annotation, animated: true)
                }
            }
            
        })
        
        mapView.delegate = self
        
    }
    
    func dismiss() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "MyPin"
        
        if annotation.isKindOfClass(MKUserLocation) {
            return nil
        }
        
        var annotationView:MKPinAnnotationView? = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
        }
        
        let leftIconView = UIImageView(frame: CGRectMake(0, 0, 53, 53))
        leftIconView.image = imageView.image
        annotationView?.leftCalloutAccessoryView = leftIconView
        
        
        return annotationView
    }
    
    
    
}

