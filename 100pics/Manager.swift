//
//  Manager\.swift
//  100pics
//
//  Created by Alexx on 16/04/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit

protocol ManagerDelegate: class {
    func managerGotList(list: NSOrderedSet)
}



class Manager: NSObject, NetworkRequestDelegate {
    let network = NetworkRequest()
    weak var delegate: ManagerDelegate?
    var photos: NSMutableOrderedSet!
    
    override init() {
        super.init()
        network.delegate = self
    }
    
    func getHomeScreenImages(imageSize: Int!) {
        let params = ["consumer_key": "n11ppo9R2ays3q8deweTKius2H2cmW6mPrQ2BPNW", "page": "1", "feature": "popular", "rpp":"50", "image_size": "\(imageSize)"]
        
        network.requestPicture(params)
    }
    
    func getNude(imageSize: Int!) {
        let params = ["consumer_key": "n11ppo9R2ays3q8deweTKius2H2cmW6mPrQ2BPNW", "page": "1", "feature": "popular", "rpp":"50","only":"Nude", "image_size": "\(imageSize)"]
        network.requestPicture(params)
        
    }
    
    func getNature(imageSize: Int!) {
        let params = ["consumer_key": "n11ppo9R2ays3q8deweTKius2H2cmW6mPrQ2BPNW", "page": "1", "feature": "popular", "rpp":"50","only":"Nature", "image_size": "\(imageSize)"]
        
        network.requestPicture(params)
        
        
    }
    
    func getLandScapes(imageSize: Int!) {
        let params = ["consumer_key": "n11ppo9R2ays3q8deweTKius2H2cmW6mPrQ2BPNW", "page": "1", "feature": "popular", "rpp":"50","only":"Landscapes", "image_size": "\(imageSize)"]
        
        network.requestPicture(params)
        
        
    }
    
    func getPhoto(imageView: UIImageView, url: String) {
        network.populatePhoto(url) { (image) in
            imageView.image = image!
        }
    }
    
    // MARK: - NetworkRequestDelegate
    func networkRequestDoneGettingList(list: NSMutableOrderedSet) {
        delegate?.managerGotList(list)
    }
}