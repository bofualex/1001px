//
//  DetailViewController.swift
//  100pics
//
//  Created by Alexx on 29/03/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit



class DetailViewController: UITableViewController {
    
    
    var detaliiPoza: Pictures!
    let manager = Manager()
    var imageCell: UIImageView!
    
    
    //MARK: lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Detalii"
        self.tableView!.reloadData()
        
    }
    // MARK: tableView methods
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "DetailCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ViewDetailCell
        
        cell.nameLabel.text = detaliiPoza.name
        cell.userName.text = detaliiPoza.username
        cell.votesCount.text = detaliiPoza.votes.description
        cell.userCity.text = detaliiPoza.city
        cell.userCountry.text = detaliiPoza.country
        cell.dataPozei.text = detaliiPoza.dataPozei
        cell.cameraModel.text = detaliiPoza.cameraModel
        manager.getPhoto(cell.thumbnailImageView2, url: detaliiPoza.url)
        imageCell = cell.thumbnailImageView2
        
        return cell
    }
    //MARK: fullscreen methods
    @IBAction func imageTapped(sender: UITapGestureRecognizer) {
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = self.view.frame
        newImageView.backgroundColor = .blackColor()
        newImageView.contentMode = .ScaleAspectFit
        newImageView.userInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(DetailViewController.dismissFullscreenImage(_:)))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
    }
    
    func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    //MARK: segue method
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showMap" {
            let destinationController = segue.destinationViewController as! MapViewController
            destinationController.imageView = imageCell
            destinationController.locatiaPozei = detaliiPoza
        }
    }
    
}