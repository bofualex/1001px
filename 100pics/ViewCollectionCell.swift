//
//  ViewCollectionCell.swift
//  100pics
//
//  Created by Alexx on 08/04/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import UIKit

class PictureCollectionCell: UICollectionViewCell{
    
    @IBOutlet var thumbnailImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        thumbnailImageView.layer.cornerRadius = 40.0
        thumbnailImageView.layer.masksToBounds = true
    }
    
        
}
