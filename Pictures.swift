//
//  Pictures.swift
//  100pics
//
//  Created by Alexx on 24/02/16.
//  Copyright © 2016 Alexx. All rights reserved.
//

import Foundation
import Alamofire



class Pictures: NSObject {
    
    let photoID: Int
    let url: String
    let name: String
    let username: String
    let latitude: Double
    let longitude: Double
    let votes: Int
    let (city, country): (String, String)
    let dataPozei: String
    let cameraModel: String
    var imageSize: Int!
    
    init(photoID: Int, url: String, name: String, username: String, latitude: Double, longitude: Double, votes: Int, city : String, country: String, dataPozei: String, cameraModel: String) {
        self.photoID = photoID
        self.url = url
        self.name = name
        self.username = username
        self.latitude = latitude
        self.longitude = longitude
        self.votes = votes
        self.city = city
        self.country = country
        self.dataPozei = dataPozei
        self.cameraModel = cameraModel
    }

    init(dictionary: NSDictionary) {
        photoID = dictionary["id"] as! Int
        url = dictionary["image_url"] as! String
        name = dictionary["name"] as! String
        username = dictionary["username"] as! String
        latitude = dictionary["latitude"] as! Double
        longitude = dictionary["longitude"] as! Double
        votes = dictionary["votes_count"] as! Int
        city = dictionary["city"] as! String
        country = dictionary["country"] as! String
        dataPozei = dictionary["created_at"] as! String
        cameraModel = dictionary["camera"] as! String
    }
    
}